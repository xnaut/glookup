### 项目介绍
广联达设计平台GLookup工具是一款.NET二次开发辅助工具：
- GLookup提供三种模式供用户在不同维度对项目各数据进行检索，分别为“文档”、“模型视图”以及“选中图元”；
- 该工具可查看图元的参数，属性，方法及其结构关系；
- 提供搜索功能，可输入关键字模糊搜索当前视图中的匹配数据；
- 提供检索历史功能，可方便用户切换历史检索数据。   

本工具操作简便，上手迅速，是广联达设计平台.NET二次开发必备工具。

交互界面：   
<img src="data/MainWindow.png" width="600px" height="auto"/>

---

### 项目架构
程序架构：  
程序使用WPF MVVM技术搭建基本结构，界面加载主要包含如下组件：  
* **GLookup Command**：在宿主程序创建插件按钮，并包含一些初始化逻辑
* **GLookup Window**：GLookup 窗口类，分三个Tab页显示不同上下文的数据，并提供搜索和历史回溯等功能
* **Snoop ViewModel**：主窗口ViewModel实现类，包含了主要的数据处理逻辑
* **Element Search Engine**：实现各种场景下数据搜索支持
* **Descriptor Builder**：数据描述器（封装api原始数据类型，对UI显示提供定制支持）生成器
* **Element Selector**：api数据查询支持
* **Api Event Handler**：api事件处理器实现

各部分大致关系如下图：  
<img src="data/Architecture.png" width="600px" height="auto"/>

源码目录结构如下图：  
<img src="data/GLookupFolders.png" width="600px" height="auto"/>

---

### 编译与运行
#### 编译环境
- 本项目运行于Windows7及以上操作系统，建议使用最新的Windows11操作系统。
- 编译本项目需要使用Microsoft Visual Studio 2019（以下简称VS）及以上，建议使用最新的Visual Studio 2022。
- 因项目依赖Roslyn，为确保编译正确须安装.NET6。

##### 安装基础依赖组件
本项目通过`<PackageReference>`方式依赖如下Nuget包，VS自动进行加载：
 * [CommunityToolkit.Mvvm](https://github.com/CommunityToolkit/dotnet)
 * [JetBrains.Annotations](https://github.com/JetBrains/JetBrains.Annotations)
 * [Microsoft.CodeAnalysis.NetAnalyzers](https://github.com/dotnet/roslyn-analyzers)
 * [Microsoft.Extensions.Hosting](https://github.com/aspnet/Hosting)
 * [Microsoft.Xaml.Behaviors.Wpf](https://github.com/microsoft/XamlBehaviorsWpf)
 * [PolySharp](https://github.com/Sergio0694/PolySharp)
 * [System.Drawing.Common](https://www.nuget.org/packages/System.Drawing.Common/)

#### 编译步骤
使用VS打开该项目，其中有以下编译配置：
- DebugGdmp & ReleaseGdmp，用于[ProjectWithQT](https://developer.glodon.com/docs/gdmp/23.2.1219.0/start/9dp014dw)。
- DebugGap & ReleaseGap，用于[GNA](https://design.glodon.com/#/productDetails/gna)。
- DebugGnuf & ReleaseGnuf
    > [!WARNING]  
    > GNUF是广联达自研UI框架，此版本不对外提供技术支持，技术支持人员不能帮助您在该配置下解决问题或为您答疑


#### 运行环境
本项目编译结果为动态链接库，即.dll文件，其作为**插件**可分别依赖于两个宿主平台：
- 广联达设计平台示例程序[ProjectWithQT](https://developer.glodon.com/docs/gdmp/23.2.1219.0/start/9dp014dw)。
- 广联达数维建筑设计[GNA](https://design.glodon.com/#/productDetails/gna)。

#### 运行步骤
- 若宿主程序为[ProjectWithQT](https://developer.glodon.com/docs/gdmp/23.2.1219.0/start/9dp014dw)，可详见[.NET SDK 入门](https://developer.glodon.com/docs/gdmp/23.2.1219.0/start/82l7fy8x)。
  
- 若宿主程序为[GNA](https://design.glodon.com/#/productDetails/gna)，用户可使用命令行REGEDIT进入注册表入口，查找以下注册表路径找到内部版本号。
    > 计算机\HKEY_LOCAL_MACHINE\SOFTWARE\Glodon\GARCH\release\0.x  

    如图所示：  
    <img src="data/GNAVersion.png" width="600px" height="auto"/>   
    在GNA安装目录中创建**ExternalAddins**文件夹与该内部版本号子文件夹，如：**ExternalAddins/410000/**。后续加载操作与[ProjectWithQT](https://developer.glodon.com/docs/gdmp/23.2.1219.0/start/82l7fy8x)中的方式一致。

---

### 项目规划
#### 迭代安排
- 规划发版：每季度一次版本发布。
- 用户自定义：对于季度内的中间版本（如客户定制版），用户可从该仓库获取源代码，替换**source/Glodon.Sdk**下的dll库进行编译生成。

### 使用交流
#### 分叉, 克隆, 创建分支和PR
1. 首先Fork仓库；
2. 克隆您的私有仓库；
3. 创建然后推送一个feature分支；
4. [创建PR](https://help.gitee.com/base/开发协作/使用PullRequest功能进行代码审查#2开发者-提交-pull-request)；
5. 处理您的修改。

请不要:
- 在同一个提交中包含大量无关修改；
- 修改与您实现的feature不直接相关的文件。

#### 需要遵守的规则
- 参考代码的编码风格进行开发；
- 修改后请务必进行调试以确保程序正确编译运行。

#### Feature和功能命名
应简洁直接，使开发者可以清晰地了解功能用途与行为边界。

---

### License
源代码授权遵循[MIT](https://opensource.org/license/mit/)协议。
详细授权信息见[License](License.md)


