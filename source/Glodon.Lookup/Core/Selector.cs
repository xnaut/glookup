// Copyright 2003-2023 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System.Windows.Threading;

#if Gdmp || Gnuf
using Glodon.Gdmp.DB;
using Glodon.Gdmp.UI;
#elif Gap
using Glodon.Gap.DB;
using Glodon.Gap.UI;
#endif
using Glodon.Lookup.Lookup.Core;
using Glodon.Lookup.Models;
using Glodon.Lookup.Services.Enums;

namespace Glodon.Lookup.Core
{
    public static class Selector
    {
        public static IReadOnlyCollection<SnoopableObject> Snoop(DataTabType tab)
        {
            return tab switch
            {
                DataTabType.ModelView => SnoopView(),
                DataTabType.Document => SnoopDocument(),
                DataTabType.Selection => SnoopSelection(),
                _ => throw new ArgumentOutOfRangeException(nameof(tab), tab, null)
            };
        }

        private static IReadOnlyCollection<SnoopableObject> SnoopView()
        {
            if (GlodonAPI.Document == null)
            {
                return Array.Empty<SnoopableObject>();
            }
            var elementsInActiveView = new ElementCollector(GlodonAPI.Document,
                    GlodonAPI.UiDocument.CurrentUIView.ModelViewId)
                    .WhereElementIsNotElementType().ToElements();

            var snoopObjsInActiveView = elementsInActiveView.Select(element => new SnoopableObject(GlodonAPI.Document, element)).ToList();
            var mergedCollection = new List<SnoopableObject>
            {
                new SnoopableObject(GlodonAPI.Document, GlodonAPI.ActiveView)
            };
            mergedCollection.AddRange(snoopObjsInActiveView);
            return mergedCollection;
        }

        private static IReadOnlyCollection<SnoopableObject> SnoopDocument()
        {
            try
            {
                if (GlodonAPI.Document == null)
                {
                    return Array.Empty<SnoopableObject>();
                }
                var mergedCollection = new List<SnoopableObject>
                {
                    new SnoopableObject(GlodonAPI.Document, GlodonAPI.Document)
                };

                var documentScopeCollection = SnoopDatabase(element => true);

                var groups = documentScopeCollection.GroupBy(e => e.Descriptor.Type);
                mergedCollection.AddRange(documentScopeCollection);
                return mergedCollection;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static IReadOnlyCollection<SnoopableObject> SnoopSelection(bool reselect = false, Dispatcher? dispatcher = null)
        {
            try
            {
                if (UIApplication.Get().ActiveUIDocument == null)
                {
                    return null;
                }

                IList<ElementId> selectedIds = UIApplication.Get().ActiveUIDocument.Selection.GetElementIds();
                if (selectedIds.Count == 0)
                {
                    if (null != GlodonAPI.PreSelection)
                    {
                        selectedIds = GlodonAPI.PreSelection;
                    }
                }
                else
                {
                    GlodonAPI.PreSelection = null;
                }
                if (selectedIds.Count == 0)
                {
                    if (reselect && dispatcher != null)
                    {
                        var snObjs = GetSelectElements(dispatcher);
                        if (snObjs.Count > 0)
                            snObjs.First().IsSelected = true;
                        return snObjs;
                    }
                    return Array.Empty<SnoopableObject>();
                }
                IList<Element> selectedElements = new ElementCollector(GlodonAPI.Document, selectedIds)
                    .WhereElementIsNotElementType().ToElements();

                if (selectedIds.Count > 0)
                {
                    return selectedElements.Select(element => new SnoopableObject(GlodonAPI.Document, element)).ToList();
                }

                selectedElements = new ElementCollector(GlodonAPI.Document, GlodonAPI.UiDocument.CurrentUIView.ModelViewId).WhereElementIsNotElementType().ToElements();
                return selectedElements.Select(element => new SnoopableObject(GlodonAPI.Document, element)).ToList();
            }
            catch (Exception e)
            {
                throw;
            }
        }
        private static IReadOnlyCollection<SnoopableObject> SnoopDatabase(Func<Element, bool> filterPredicate)
        {
            var elementTypes = new ElementCollector(GlodonAPI.Document).WhereElementIsElementType();
            var elementInstances = new ElementCollector(GlodonAPI.Document).WhereElementIsNotElementType();

            return elementTypes
                .UnionWith(elementInstances).Where(filterPredicate)
                .Select(element => new SnoopableObject(GlodonAPI.Document, element))
                .ToList();
        }

        private static IReadOnlyCollection<SnoopableObject> GetSelectElements(Dispatcher dispatcher)
        {
            var nodeReferences = dispatcher.Invoke(() =>
            GlodonAPI.UiDocument.Selection.PickObjects(new PickTarget(PickTargetType.GraphicsElementShape)));
            var elementIds = new List<ElementId>();
            var objectList = new List<SnoopableObject>();
            foreach (var refrence in nodeReferences)
            {
                var element = GlodonAPI.Document.GetElement(refrence.ElementId);
                objectList.Add(new SnoopableObject(GlodonAPI.Document, element));
                elementIds.Add(refrence.ElementId);
            }
            GlodonAPI.UiDocument.Selection.SetElementIds(elementIds);

            return objectList;
        }
    }
}
