// Copyright 2003-2023 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Markup;
using System.Windows.Threading;

#if Gdmp || Gnuf
using Glodon.Gdmp.DB;
#elif Gap
using Glodon.Gap.DB;
#endif
using Glodon.Lookup.Core.ModelBase;
using Glodon.Lookup.Models.Descriptors;

namespace Glodon.Lookup.Core
{
    public static class DescriptorMap
    {
        /// <summary>
        /// 通过模糊或者精确的匹配查找一个Descriptor
        /// </summary>
        public static Descriptor? FindDescriptor(object obj, Type type)
        {
            return obj switch
            {
                //System
                string value when type is null || type == typeof(string) => new StringDescriptor(value),
                bool value when type is null || type == typeof(bool) => new BoolDescriptor(value),
                IEnumerable value => new EnumerableDescriptor(value),
                Exception value when type is null || type == typeof(Exception) => new ExceptionDescriptor(value),

                //Root
                ElementId value when type is null || type == typeof(ElementId) => new ElementIdDescriptor(value),
                UniIdentity value when type is null || type == typeof(UniIdentity) => new UniIdentityDescriptor(value),

                //Enumerator
                IEnumerator value => new EnumeratorDescriptor(value),

                //APIObjects
                Category value when type is null || type == typeof(Category) => new CategoryDescriptor(value),
                Parameter value when type is null || type == typeof(Parameter) => new ParameterDescriptor(value),
                Reference value when type is null || type == typeof(Reference) => new ReferenceDescriptor(value),
                Color value when type is null || type == typeof(Color) => new ColorDescriptor(value),
                Curve value when type is null || type == typeof(Curve) => new CurveDescriptor(value),
                Edge value when type is null || type == typeof(Edge) => new EdgeDescriptor(value),
                Face value when type is null || type == typeof(Face) => new FaceDescriptor(value),

                //IDisposables
                FamilyInstance value when type is null || type == typeof(FamilyInstance) => new FamilyInstanceDescriptor(value),
                Element value when type is null || type == typeof(Element) => new ElementDescriptor(value),
                Document value when type is null || type == typeof(Document) => new DocumentDescriptor(value),

                IDisposable when type is null || type == typeof(IDisposable) => new ApiObjectDescriptor(),

                //Internal
                ResolveSet value => new ResolveSetDescriptor(value),
                ResolveSummary value => new ResolveSummaryDescriptor(value),

                //ComponentManager
                UIElement => new UiElementDescriptor(),
                DispatcherObject => new DependencyObjectDescriptor(),
                INotifyPropertyChanged => new UiObjectDescriptor(),

                //Unknown
                null when type is null => new ObjectDescriptor(),
                _ when type is null => new ObjectDescriptor(obj),
                _ => null
            };
        }
    }
}
