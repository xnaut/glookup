//////////////////////////////////////////////////////////////////////////////
//
// Copyright © 1998-2024 Glodon.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////

#if Gdmp || Gnuf
using Glodon.Gdmp.UI;
#elif Gap
using Glodon.Gap.UI;
#endif

namespace Glodon.Lookup.Utils
{
    public class ExceptionUtils
    {
        public static void ShowExceptionMessage(Exception e)
        {
#if Gdmp 
            MessageBoard.Show(e.Message + e.StackTrace, "GLookup");
#elif Gap || Gnuf
            MessageBoard.Show(e.Message + e.StackTrace, "GLookup", true, MessageBoardButton.Ok);
#endif
        }

        public static void ShowExceptionMessage(string message)
        {
#if Gdmp
            MessageBoard.Show(message, "GLookup");
#elif Gap || Gnuf
            MessageBoard.Show(message, "GLookup", true, MessageBoardButton.Ok);
#endif
        }
    }
}
