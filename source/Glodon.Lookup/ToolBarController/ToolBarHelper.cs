// Copyright 2003-2023 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System.Reflection;

#if Gdmp || Gnuf
using Glodon.Gdmp.UI;
#elif Gap
using Glodon.Gap.UI;
#endif

namespace Glodon.Lookup.ToolBarController
{
    public static class ToolBarHelper
    {
        private const string IMAGEDIRECTORY = @"Resources\Images";
        public static void CreateBlock(UIApplication application)
        {
            var addinBlock = application.AddBlock("Glodon Lookup");
            var dropdown = addinBlock.AddDropdownButton("Glodon.Lookup", "Glodon.Lookup", IMAGEDIRECTORY);
        }
        public static Block AddBlock(this UIApplication uIApplication, string blockName, string tabName = "AddInsTab")
        {
            Block? block = null;
            WorkspaceTab? tab = null;
            foreach (var existedTab in uIApplication.GetWorkspaceBar().GetTabs())
            {
                if (existedTab.Name.Equals(tabName))
                {
                    tab = existedTab;
                }
            }
            if (tab == null)
            {
                tab = uIApplication.GetWorkspaceBar().AddTab(tabName, "AddIns");
            }
            foreach (var existedBlock in uIApplication.GetMainToolBar().GetBlocks(tabName))
            {
                if (existedBlock.Name.Equals(blockName))
                {
                    block = existedBlock;
                }
            }
            if (block == null)
            {
                block = uIApplication.GetMainToolBar().AddBlock(tabName);
            }
            return block;
        }

        public static DropdownButton AddDropdownButton(this Block block, string name, string caption, string imageDir)
        {
            var dropdownData = new DropdownButtonData(name, new List<ControlDataBase>());
            dropdownData.Caption = caption;
            dropdownData.ImagesDirectory = imageDir;
            return block.AddItem(dropdownData) as DropdownButton;
        }

        public static PushButton AddPushButton<GCommand>(this DropdownButton dropdown, string name, string caption, string imageDir, string toolTip = "", string description = "") where GCommand : IExternalCommand, new()
        {
            Type typeFromHandle = typeof(GCommand);
            var pushButtonData = new PushButtonData(
                name, caption, Assembly.GetAssembly(typeFromHandle).Location, typeFromHandle.FullName);
            pushButtonData.ImagesDirectory = imageDir;
            return dropdown.AddPushButton(pushButtonData);
        }
    }
}
