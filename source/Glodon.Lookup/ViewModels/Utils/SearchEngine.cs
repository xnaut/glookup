//////////////////////////////////////////////////////////////////////////////
//
// Copyright © 1998-2024 Glodon.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////

#if Gdmp || Gnuf
using Glodon.Gdmp.DB;
#elif Gap
using Glodon.Gap.DB;
#endif
using Glodon.Lookup.Core.ModelBase;
using Glodon.Lookup.Models;
using Glodon.Lookup.ViewModels.Contracts;
using Glodon.Lookup.ViewModels.Enums;
using Glodon.Lookup.ViewModels.Objects;
using Glodon.Lookup.ViewModels.Pages.Tracebale;

namespace Glodon.Lookup.ViewModels.Utils
{
    public static class SearchEngine
    {
        private static SearchResults Search(ISnoopViewModel viewModel, SearchOption option)
        {
            try
            {
                IEnumerable<SnoopableObject> docData = Array.Empty<SnoopableObject>();
                ITabGroupViewModel<SnoopHistoryViewModel>? tabGroupViewModel = viewModel as ITabGroupViewModel<SnoopHistoryViewModel>;
                var docVm = tabGroupViewModel!.GetVmByTab(Services.Enums.DataTabType.Document);
                var currentVm = tabGroupViewModel.CurrentViewModel;
                if (docVm.SnoopHistory.Any())
                {
                    docData = docVm.SnoopHistory.First().Objects;
                }
                return null;

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public static async Task<SearchResults> SearchAsync(ISnoopViewModel model, SearchOption option, CancellationToken cancellationToken)
        {
            return await Task.Run(() => Search(model, option), cancellationToken);
        }


        private static List<SnoopableObject> Search(SnoopableObject query, IEnumerable<SnoopableObject> data)
        {
            var filteredSnoopableObjects = new List<SnoopableObject>();
            foreach (var item in data)
                if (item.Descriptor.Type == query.Descriptor.Type)
                    filteredSnoopableObjects.Add(item);

            return filteredSnoopableObjects;
        }


        private static List<SnoopableObject> Search(string query, IEnumerable<SnoopableObject> data, IEnumerable<SnoopableObject> documentData)
        {
            var filteredSnoopableObjects = new List<SnoopableObject>();
            foreach (var item in data)
            {
                if (item.Descriptor.Name.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0)
                    filteredSnoopableObjects.Add(item);
                else if (item.Descriptor.Type.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0)
                    filteredSnoopableObjects.Add(item);
            }

            foreach (var item in documentData)
            {
                if (MatchId(item, query) && !filteredSnoopableObjects.Any(s => s.Object is Element e && e.Id.Equals((item.Object as Element)?.Id)))
                    filteredSnoopableObjects.Add(item);
            }

            return filteredSnoopableObjects;
        }

        public static bool MatchId(SnoopableObject snoopObj, string query)
        {
            var delimiters = new[] { '\t', ';', ',', ' ' };
            var rows = query.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);

            var items = new List<string>(rows.Length);
            foreach (var row in rows)
                for (var i = 0; i < delimiters.Length; i++)
                {
                    var delimiter = delimiters[i];
                    var split = row.Split(new[] { delimiter }, StringSplitOptions.RemoveEmptyEntries);
                    if (split.Length > 1 || i == delimiters.Length - 1 || split.Length == 1 && split[0] != row)
                    {
                        items.AddRange(split);
                        break;
                    }
                }

            foreach (var rawId in items)
            {
                if (long.TryParse(rawId, out var id))
                {
                    if (snoopObj.Object is Element ele && ele.IsValidObject() && ele.Id.Equals(new ElementId(id)))
                        return true;
                }
                else if (rawId.Length == 36 && rawId.Count(c => c == '-') == 4)
                {
                    if (snoopObj.Object is Element ele && ele.IsValidObject() && ele.UId.Guid.ToString().Equals(rawId))
                        return true;
                }
            }
            return false;
        }


        private static List<Descriptor> Search(string query, IEnumerable<Descriptor> data)
        {
            var filteredSnoopableData = new List<Descriptor>();
            foreach (var item in data)
                if (item.Name.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0) filteredSnoopableData.Add(item);
                else if (item.Value.Descriptor.Name.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0) filteredSnoopableData.Add(item);

            return filteredSnoopableData;
        }
    }
}
