﻿//////////////////////////////////////////////////////////////////////////////
//
// Copyright © 1998-2024 Glodon.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////

using System.Windows;

using Microsoft.Extensions.DependencyInjection;

using Glodon.Lookup.Lookup.Core;
using Glodon.Lookup.Services.Contracts;
using Glodon.Lookup.Services.Enums;
using Glodon.Lookup.ViewModels.Contracts;
using Glodon.Lookup.Utils;

#if Gdmp || Gnuf
#elif Gap
using Glodon.Gap.UI;
#endif
using Glodon.Lookup.ViewModels.Pages.Tracebale;

namespace Glodon.Lookup.Commands
{
    public class GLookupCommand : CommandBase
    {
        private IWindow? _lookupWindow;


        protected override void Execute()
        {
            try
            {
                _lookupWindow = Host.GetService<IWindow>();
                //如果窗口已经隐藏了（用户点击了关闭按钮），需要重新获取数据
                if (_lookupWindow is Window window && (window.Visibility == Visibility.Hidden || window.Visibility == Visibility.Collapsed))
                {
                    ISnoopService? snoopService = _lookupWindow.Scope.GetService<ISnoopService>();
                    ITabGroupViewModel<SnoopHistoryViewModel>? snoopViewModel = _lookupWindow.Scope.GetService<ITabGroupViewModel<SnoopHistoryViewModel>>();
                    GlodonAPI.RegistHandlers((snoopViewModel as IApiEventHandler)!);

                    _ = snoopService!.Snoop(DataTabType.Document);
                    _ = snoopService!.Snoop(DataTabType.ModelView);
                    _ = snoopService!.Snoop(DataTabType.Selection);

                    GlodonAPI.PreSelection = GlodonAPI.UiDocument.Selection.GetElementIds();
                    if (GlodonAPI.PreSelection.Count > 0)
                    {
                        snoopViewModel!.CurrentViewModel = snoopViewModel.GetVmByTab(DataTabType.Selection);
                    }
                }
                //最小化或者隐藏后重新显示窗口
                _lookupWindow.Scope.GetService<IWindowController>()!.Show();


            }
            catch (Exception ex)
            {
                ExceptionUtils.ShowExceptionMessage(ex);
            }
        }
    }
}
