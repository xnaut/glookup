// Copyright 2003-2023 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System.Reflection;

#if Gdmp || Gnuf
using Glodon.Gdmp.DB;
#elif Gap
using Glodon.Gap.DB;
#endif
using Glodon.Lookup.Core.Contracts;
using Glodon.Lookup.Core.ModelBase;

namespace Glodon.Lookup.Models.Descriptors
{
    public sealed class CategoryDescriptor : Descriptor, IDescriptorResolver, IDescriptorExtension
    {
        private readonly Category _category;
        private string? _builtinCatgoryName;
        public CategoryDescriptor(Category category)
        {
            _category = category;
            Name = category.Name;
        }
        private bool IsBuitInCategory()
        {
            var properties = typeof(BuiltInCategory).GetProperties(BindingFlags.Static | BindingFlags.Public);
            foreach (var prop in properties)
            {
                UniIdentity? uid = prop.GetValue(null) as UniIdentity;
                if (_category.UId.Equals(uid))
                {
                    _builtinCatgoryName = prop.Name;
                    return true;
                }
            }
            return false;
        }
        public ResolveSet Resolve(Document context, string target, ParameterInfo[] parameters)
        {
            try
            {
                return target switch
                {
                    "Name" => ResolveSet.Append(_category.Name, "Category Name"),
                    "UId" => ResolveSet.Append(_category.UId, "Category UId"),
                    "ParentUId" => ResolveSet.Append(_category.ParentUId, "Category ParentUId"),
                    _ => null
                };
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public ResolveSet Resolve(Document context, string target, params object[] parameterObjs)
        {
            return null;
        }

        public void RegisterExtensions(IExtensionManager manager)
        {
            if (IsBuitInCategory())
            {
                manager.Register(_category, extension =>
                {
                    extension.Name = nameof(IsBuitInCategory);
                    extension.Result = true;
                });
                manager.Register(_category, extension =>
                {
                    extension.Name = nameof(BuiltInCategory) + " name";
                    extension.Result = _builtinCatgoryName;
                });
            }
            else
            {
                manager.Register(_category, extension =>
                {
                    extension.Name = nameof(IsBuitInCategory);
                    extension.Result = false;
                });
            }
        }
    }
}
