// Copyright 2003-2023 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System.Collections;
using System.Reflection;

#if Gdmp || Gnuf
using Glodon.Gdmp.DB;
#elif Gap
using Glodon.Gap.DB;
#endif
using Glodon.Lookup.Core.Contracts;
using Glodon.Lookup.Core.ModelBase;

namespace Glodon.Lookup.Models.Descriptors
{
    public class EnumerableDescriptor : Descriptor, IDescriptorEnumerator, IDescriptorResolver
    {
        public EnumerableDescriptor(IEnumerable value)
        {
            Enumerator = value.GetEnumerator();

            //Checking types to reduce memory allocation when creating an iterator and increase performance
            switch (value.GetType().Name)
            {
                case nameof(String):
                    IsEmpty = true;
                    break;
                case nameof(ICollection):
                    IsEmpty = (value as ICollection).Count == 0;
                    break;
                case nameof(HashSet<ElementId>):
                    IsEmpty = (value as HashSet<ElementId>).Count == 0;
                    break;
                default:
                    IsEmpty = !Enumerator.MoveNext();
                    break;
            }

            Name = IsEmpty ? "Empty" : Name;
        }

        public IEnumerator Enumerator { get; }
        public bool IsEmpty { get; }

        public ResolveSet Resolve(Document context, string target, ParameterInfo[] parameters)
        {
            ResolveSet resolveSet = null;
            switch (target.GetType().Name)
            {
                case nameof(IEnumerable.GetEnumerator):
                    resolveSet = ResolveSet.Append(null);
                    break;
                default:
                    break;
            }
            return resolveSet;
        }

        public ResolveSet Resolve(Document context, string target, params object[] parameterObjs)
        {
            return null;
        }
    }
}
