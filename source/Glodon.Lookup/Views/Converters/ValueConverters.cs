//////////////////////////////////////////////////////////////////////////////
//
// Copyright © 1998-2023 Glodon.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////

using Glodon.Lookup.Models;
using Glodon.Lookup.ViewModels.Objects;
using Glodon.Lookup.Views.Markup;

using System.Globalization;
using System.Windows.Data;

namespace Glodon.Lookup.Views.Converters
{
    public sealed class NumRateConverter : SelfProvider, IValueConverter
    {
        public object Convert(object value, Type targetType, object rate, CultureInfo culture)
        {
            double? v = value as double?;
            return v * double.Parse(rate.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }

    public sealed class NumAddingConverter : SelfProvider, IValueConverter
    {
        public object Convert(object value, Type targetType, object addNum, CultureInfo culture)
        {
            double? v = value as double?;
            return v + double.Parse(addNum.ToString());
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class SnoopAndHisCountToBoolConverter : SelfProvider, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int hisCount = (int)value;
            return hisCount > 1;
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

    }
    public sealed class SelectedObjectToPathNodeConverter : SelfProvider, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (value is SnoopableObject sn)
                {
                    return sn.Descriptor.Name;
                }
                return "#未选中任何对象";
            }
            catch (Exception e)
            {
                return "从Action解析路径失败";
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
    public sealed class SnoopActionToIndexConverter : SelfProvider, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            var snoopAction = values[0] as SnoopAction;
            var snoopHistory = values[1] as IList<SnoopAction>;
            if (snoopHistory!.Contains(snoopAction!))
            {
                int index = snoopHistory.IndexOf(snoopAction);
                return index + 1;
            }
            return -1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
