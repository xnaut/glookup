//////////////////////////////////////////////////////////////////////////////
//
// Copyright © 1998-2024 Glodon.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the “Software”),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
// sell copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
// THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
//////////////////////////////////////////////////////////////////////////////

using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Markup;


#if Gdmp || Gnuf
#elif Gap
using Glodon.Gap.UI;
#endif
using Glodon.Lookup.Core.ModelBase;
using Glodon.Lookup.Models;
using Glodon.Lookup.Services.Enums;
using Glodon.Lookup.Utils;
using Glodon.Lookup.Views.Markup;

namespace Glodon.Lookup.Views.Converters
{
    public sealed class TreeViewGroupConverter : MarkupExtension, IMultiValueConverter
    {
        private bool IsTabObject(SnoopableObject snoopableObject, DataTabType dataTab)
        {
            return snoopableObject.Descriptor.Type.Equals(dataTab.ToString(), StringComparison.Ordinal) &&
                            (dataTab == DataTabType.Document || dataTab == DataTabType.ModelView);
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            try
            {
                if (values[0] == null || values[1] == null)
                {
                    return null;
                }
                var tab = (DataTabType)values[0];

                IReadOnlyCollection<SnoopableObject>? source = values[1] as IReadOnlyCollection<SnoopableObject>;

                var viewSource = new CollectionViewSource
                {
                    Source = source
                };
                //对分组进行自定义排序
                (viewSource.View as ListCollectionView)!.CustomSort = Comparer<object>.Create((x, y) =>
                {
                    try
                    {
                        if (x is not SnoopableObject xs || y is not SnoopableObject ys)
                        {
                            throw new ArgumentException($"ListCollectionView自定义排序只支持{nameof(SnoopableObject)}");
                        }
                        if (IsTabObject(xs, tab) && IsTabObject(ys, tab))
                        {
                            return 0;
                        }
                        if (IsTabObject(xs, tab))
                        {
                            return -1;
                        }
                        else if (IsTabObject(ys, tab))
                        {
                            return 1;
                        }
                        else
                        {
                            if (xs.Descriptor.Type.CompareTo(ys.Descriptor.Type) != 0)
                            {
                                return xs.Descriptor.Type.CompareTo(ys.Descriptor.Type);
                            }
                            if (xs.Descriptor.Name.CompareTo(ys.Descriptor.Name) != 0)
                            {
                                return xs.Descriptor.Name.CompareTo(ys.Descriptor.Name);
                            }
                            if (xs.Descriptor.Description != null && ys.Descriptor.Description != null
                             && xs.Descriptor.Description.CompareTo(ys.Descriptor.Description) != 0)
                            {
                                return xs.Descriptor.Description.CompareTo(ys.Descriptor.Description);
                            }
                            else
                            {
                                if (xs.Descriptor.Description != null)
                                {
                                    return -1;
                                }
                                if (ys.Descriptor.Description != null)
                                {
                                    return 1;
                                }
                            }
                        }
                        return 0;
                    }
                    catch (Exception e)
                    {
                        ExceptionUtils.ShowExceptionMessage(e);
                        return 0;
                    }
                });
                viewSource.GroupDescriptions.Add(new PropertyGroupDescription("Descriptor.Type"));
                if (viewSource.Source is IEnumerable<SnoopableObject> enumerable)
                {
                    if (enumerable.Any() && !enumerable.Any(s => s.IsSelected))
                    {
                        var firstGroup = viewSource.View.Groups.First() as CollectionViewGroup;
                        if (firstGroup != null && firstGroup.Items.Any())
                        {
                            SnoopableObject? snoopableObject = firstGroup.Items.First() as SnoopableObject;
                            if (snoopableObject is not null)
                            {
                                snoopableObject.IsSelected = true;
                            }
                        }
                    }
                }
                return viewSource.View.Groups;
            }
            catch (Exception e)
            {
                ExceptionUtils.ShowExceptionMessage(e);
                return Array.Empty<object>();
            }
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }


    public sealed class GroupToIndexConverter : MarkupExtension, IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values[0] is CollectionViewGroup)
            {
                CollectionViewGroup group = (CollectionViewGroup)values[0];
                ICollection<object> groups = values[1] as ICollection<object>;
                if (groups != null)
                {
                    for (int i = 0; i < groups.Count; i++)
                    {
                        CollectionViewGroup? collectionViewGroup = groups.ElementAt(i) as CollectionViewGroup;
                        if (collectionViewGroup != null && collectionViewGroup.Equals(group))
                        {
                            return i;
                        }
                    }
                }
            }
            return -1;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public sealed class DescriptorToVisibilityConverter : MarkupExtension, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            Descriptor descriptor = (Descriptor)value;
            if (descriptor == null)
            {
                return Visibility.Visible;
            }
            if (descriptor.Type.Equals("Document") || descriptor.Type.Equals("ModelView"))
            {
                return Visibility.Collapsed;
            }
            else
            {
                return Visibility.Hidden;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public override object ProvideValue(IServiceProvider serviceProvider)
        {
            return this;
        }
    }

    public sealed class ItemsSelectedToExpandedConverter : SelfProvider, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            var items = value as ICollection<object>;
            if (items == null)
            {
                return false;
            }
            else
            {
                var any = items.Any(s => s is SnoopableObject sn && sn.IsSelected == true);
                return any;
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public sealed class DataContextToFocusableConverter : SelfProvider, IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is SnoopableObject)
            {
                return true;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

}
