// Copyright 2003-2023 by Autodesk, Inc. 
//
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted, 
// provided that the above copyright notice appears in all copies and 
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting 
// documentation.
//
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS. 
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC. 
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
//
// Use, duplication, or disclosure by the U.S. Government is subject to 
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using Glodon.Lookup.Services.Contracts;

using System.Windows;
using System.Windows.Markup;

using Wpf.Ui.Appearance;

namespace Glodon.Lookup.Views.Markup
{

    [Localizability(LocalizationCategory.Ignore)]
    [Ambient]
    [UsableDuringInitialization(true)]
    public sealed class ThemesDictionary : ResourceDictionary
    {
        public ThemesDictionary()
        {
#if DEBUG
            ThemeType theme;
            try
            {
                theme = Host.GetService<ISettingsService>().Theme;
            }
            catch
            {
                //UnHosted build
                theme = ThemeType.Light;
            }
#else
            var theme = Host.GetService<ISettingsService>().Theme;
#endif
            string themeName;

            switch (theme)
            {
                case ThemeType.Dark:
                    themeName = "Dark";
                    break;
                case ThemeType.HighContrast:
                    themeName = "HighContrast";
                    break;
                default:
                    themeName = "Light";
                    break;
            }
            Source = new Uri($"{AppearanceData.LibraryThemeDictionariesUri}{themeName}.xaml", UriKind.Absolute);
        }
    }

    public sealed class CustomThemesDictionary : ResourceDictionary
    {
        private const string CustomThemeDictionariesUri = "pack://application:,,,/Glodon.Lookup;component/Styles/Theme/";
        public CustomThemesDictionary()
        {
#if DEBUG
            ThemeType theme;
            try
            {
                theme = Host.GetService<ISettingsService>().Theme;
            }
            catch
            {
                //UnHosted build
                theme = ThemeType.Light;
            }
#else
            var theme = Host.GetService<ISettingsService>().Theme;
#endif
            string themeName;

            switch (theme)
            {
                case ThemeType.Dark:
                    themeName = "Dark";
                    break;
                default:
                    themeName = "Light";
                    break;
            }
            Source = new Uri($"{CustomThemeDictionariesUri}{themeName}.xaml", UriKind.Absolute);
        }
    }
}
