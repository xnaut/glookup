﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Windows.Data;
using Wpf.Ui.Common;
using Wpf.Ui.Extensions;

namespace Wpf.Ui.Converters;


internal class ObjectToSymbolConverter : IValueConverter
{
    
    /// <returns>Valid <see cref="SymbolRegular"/> or <see cref="SymbolRegular.Empty"/> if failed.</returns>
    public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        if (value is SymbolRegular symbol)
            return symbol;

        if (value is SymbolFilled symbolFilled)
            return symbolFilled.Swap();

        return SymbolRegular.Empty;
    }

    
    /// <exception cref="NotImplementedException"></exception>
    public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}

