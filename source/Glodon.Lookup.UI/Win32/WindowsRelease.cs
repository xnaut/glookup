﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

namespace Wpf.Ui.Win32;


internal enum WindowsRelease
{
    
    Windows95 = 950, // The good old days

    
    Windows98 = 1998,

    
    Windows2000 = 2195,

    
    WindowsXP = 2600,

    
    WindowsMe = 3000,

    
    Windows7 = 7600,

    
    Windows7Sp1 = 7601,

    
    Windows8 = 9200,

    
    Windows81 = 9600,

    
    Windows10 = 10240,

    
    Windows10V1511 = 10586,

    
    Windows10V1607 = 14393,

    
    Windows10V1703 = 15063,

    
    Windows10V1809 = 17763,

    
    Windows10V19H1 = 18362,

    
    Windows10Insider1 = 18985,

    
    Windows10V20H1 = 19041,

    
    Windows10V20H2 = 19042,

    
    Windows10V21H1 = 19043,

    
    Windows10V21H2 = 19044,

    
    WindowsServer2022 = 20348,

    
    Windows11 = 22000,

    
    Windows11Insider1 = 22523,

    
    Windows11Insider2 = 22557,
}
