﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Markup;

[assembly: ComVisible(false)]
[assembly: ThemeInfo(ResourceDictionaryLocation.None, ResourceDictionaryLocation.SourceAssembly)]

[assembly: XmlnsPrefix("http://glodon.lookup.com/xaml", "rl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Common")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Markup")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Converters")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.Navigation")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.Window")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.AutoSuggestBoxControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.TreeGridControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.VirtualizingControls")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.TitleBarControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.ThumbRateControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.NumberBoxControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.MessageBoxControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.ContentDialogControl")]
[assembly: XmlnsDefinition("http://glodon.lookup.com/xaml", "Wpf.Ui.Controls.BreadcrumbControl")]
