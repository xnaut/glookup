﻿ 
// Copyright 2003-2023 by Autodesk, Inc.
// 
// Permission to use, copy, modify, and distribute this software in
// object code form for any purpose and without fee is hereby granted,
// provided that the above copyright notice appears in all copies and
// that both that copyright notice and the limited warranty and
// restricted rights notice below appear in all supporting
// documentation.
// 
// AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
// AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
// MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC.
// DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
// UNINTERRUPTED OR ERROR FREE.
// 
// Use, duplication, or disclosure by the U.S. Government is subject to
// restrictions set forth in FAR 52.227-19 (Commercial Computer
// Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
// (Rights in Technical Data and Computer Software), as applicable.

using System.Collections;
using System.Windows;

namespace Wpf.Ui.Controls;

public class TreeView : System.Windows.Controls.TreeView
{
    protected override void OnItemsSourceChanged(IEnumerable oldValue, IEnumerable newValue)
    {
        base.OnItemsSourceChanged(oldValue, newValue);
        ItemsSourceChanged?.Invoke(this, newValue);
    }

    public event EventHandler<IEnumerable> ItemsSourceChanged;

    public static Visibility GetExpanderVisibility(UIElement target) =>
        (Visibility)target.GetValue(ExpanderVisibilityProperty);
    public static void SetExpanderVisibility(UIElement target, Visibility value) =>
        target.SetValue(ExpanderVisibilityProperty, value);
    // Using a DependencyProperty as the backing store for ExpanderVisibility.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ExpanderVisibilityProperty =
        DependencyProperty.RegisterAttached("ExpanderVisibility", typeof(Visibility), typeof(TreeView), new PropertyMetadata(Visibility.Visible));

    public static Visibility GetExpanderToggleButtonVisibility(UIElement target) =>
        (Visibility)target.GetValue(ExpanderToggleButtonVisibilityProperty);
    public static void SetExpanderToggleButtonVisibility(UIElement target, Visibility value) =>
        target.SetValue(ExpanderToggleButtonVisibilityProperty, value);

    // Using a DependencyProperty as the backing store for ExpanderToggleButtonVisibility.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty ExpanderToggleButtonVisibilityProperty =
        DependencyProperty.Register("ExpanderToggleButtonVisibility", typeof(Visibility), typeof(TreeView), new PropertyMetadata(Visibility.Visible));


}