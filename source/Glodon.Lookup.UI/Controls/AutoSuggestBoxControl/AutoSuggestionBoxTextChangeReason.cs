﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

namespace Wpf.Ui.Controls.AutoSuggestBoxControl;

public enum AutoSuggestionBoxTextChangeReason
{
    
    UserInput = 0,
    ProgrammaticChange = 1,
    SuggestionChosen = 2,
}
