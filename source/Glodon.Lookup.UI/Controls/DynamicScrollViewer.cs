﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.ComponentModel;
using System.Drawing;
using System.Windows;
using System.Windows.Controls;
using Wpf.Ui.Common;

namespace Wpf.Ui.Controls;


[ToolboxItem(true)]
[ToolboxBitmap(typeof(DynamicScrollViewer), "DynamicScrollViewer.bmp")]
[DefaultEvent("ScrollChangedEvent")]
public class DynamicScrollViewer : System.Windows.Controls.ScrollViewer
{
    private readonly EventIdentifier _verticalIdentifier = new();

    private readonly EventIdentifier _horizontalIdentifier = new();

    // Due to the large number of triggered events, we limit the complex logic of DependencyProperty
    private bool _scrollingVertically = false;

    private bool _scrollingHorizontally = false;

    private int _timeout = 1200;

    private double _minimalChange = 40d;

    
    public static readonly DependencyProperty IsScrollingVerticallyProperty = DependencyProperty.Register(
        nameof(IsScrollingVertically),
        typeof(bool), typeof(DynamicScrollViewer),
        new PropertyMetadata(false, IsScrollingVerticallyProperty_OnChanged));

    
    public static readonly DependencyProperty IsScrollingHorizontallyProperty = DependencyProperty.Register(
        nameof(IsScrollingHorizontally),
        typeof(bool), typeof(DynamicScrollViewer), new PropertyMetadata(false, IsScrollingHorizontally_OnChanged));

    
    public static readonly DependencyProperty MinimalChangeProperty = DependencyProperty.Register(
        nameof(MinimalChange),
        typeof(double), typeof(DynamicScrollViewer), new PropertyMetadata(40d, MinimalChangeProperty_OnChanged));

    
    public static readonly DependencyProperty TimeoutProperty = DependencyProperty.Register(nameof(Timeout),
        typeof(int), typeof(DynamicScrollViewer), new PropertyMetadata(1200, TimeoutProperty_OnChanged));

    // Using a DependencyProperty as the backing store for MyProperty.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty BottomBarProperty =
        DependencyProperty.Register(nameof(BottomBar), typeof(FrameworkElement), typeof(DynamicScrollViewer), new PropertyMetadata());

    
    public bool IsScrollingVertically
    {
        get => (bool)GetValue(IsScrollingVerticallyProperty);
        set => SetValue(IsScrollingVerticallyProperty, value);
    }

    
    public bool IsScrollingHorizontally
    {
        get => (bool)GetValue(IsScrollingHorizontallyProperty);
        set => SetValue(IsScrollingHorizontallyProperty, value);
    }

    
    public double MinimalChange
    {
        get => (double)GetValue(MinimalChangeProperty);
        set => SetValue(MinimalChangeProperty, value);
    }

    
    public int Timeout
    {
        get => (int)GetValue(TimeoutProperty);
        set => SetValue(TimeoutProperty, value);
    }

    //protected override void OnPreviewMouseWheel(MouseWheelEventArgs e)
    //{
    //    base.OnPreviewMouseWheel(e);
    //}

    //protected override void OnKeyDown(KeyEventArgs e)
    //{
    //    base.OnKeyDown(e);
    //}



    public FrameworkElement BottomBar
    {
        get { return (FrameworkElement)GetValue(BottomBarProperty); }
        set { SetValue(BottomBarProperty, value); }
    }

    
    /// <remarks>
    /// OnScrollChanged fires the ScrollChangedEvent. Overriders of this method should call
    /// base.OnScrollChanged(args) if they want the event to be fired.
    /// </remarks>
    /// <param name="e">ScrollChangedEventArgs containing information about the change in scrolling state.</param>
    protected override void OnScrollChanged(ScrollChangedEventArgs e)
    {
        base.OnScrollChanged(e);

        //#if DEBUG
        //            System.Diagnostics.Debug.WriteLine($"DEBUG | {typeof(DynamicScrollBar)}.{nameof(e.VerticalChange)} - {e.VerticalChange}", "WPFUI");
        //            System.Diagnostics.Debug.WriteLine($"DEBUG | {typeof(DynamicScrollBar)}.{nameof(e.HorizontalChange)} - {e.HorizontalChange}", "WPFUI");
        //#endif

        if (e.HorizontalChange > _minimalChange || e.HorizontalChange < -_minimalChange)
            UpdateHorizontalScrollingState();

        if (e.VerticalChange > _minimalChange || e.VerticalChange < -_minimalChange)
            UpdateVerticalScrollingState();
    }

    private async void UpdateVerticalScrollingState()
    {
        // TODO: Optimize
        // My main assumption here is that each scroll causes a new "event / thread" to be assigned.
        // If more than Timeout has passed since the last event, there is no interaction.
        // We pass this value to the ScrollBar and link it to IsMouseOver.
        // This way we have a dynamic scrollbar that responds to scroll / mouse over.

        var currentEvent = _verticalIdentifier.GetNext();

        if (!_scrollingVertically)
            IsScrollingVertically = true;

        if (_timeout > -1)
            await Task.Delay(_timeout < 10000 ? _timeout : 1000);

        if (_verticalIdentifier.IsEqual(currentEvent) && _scrollingVertically)
            IsScrollingVertically = false;
    }

    private async void UpdateHorizontalScrollingState()
    {
        // TODO: Optimize
        // My main assumption here is that each scroll causes a new "event / thread" to be assigned.
        // If more than Timeout has passed since the last event, there is no interaction.
        // We pass this value to the ScrollBar and link it to IsMouseOver.
        // This way we have a dynamic scrollbar that responds to scroll / mouse over.

        var currentEvent = _horizontalIdentifier.GetNext();

        if (!_scrollingHorizontally)
            IsScrollingHorizontally = true;

        await Task.Delay(Timeout < 10000 ? Timeout : 1000);

        if (_horizontalIdentifier.IsEqual(currentEvent) && _scrollingHorizontally)
            IsScrollingHorizontally = false;
    }

    private static void IsScrollingVerticallyProperty_OnChanged(DependencyObject d,
        DependencyPropertyChangedEventArgs e)
    {
        if (d is not DynamicScrollViewer scroll)
            return;

        scroll._scrollingVertically = scroll.IsScrollingVertically;
    }

    private static void IsScrollingHorizontally_OnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not DynamicScrollViewer scroll)
            return;

        scroll._scrollingHorizontally = scroll.IsScrollingHorizontally;
    }

    private static void MinimalChangeProperty_OnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not DynamicScrollViewer scroll)
            return;

        scroll._minimalChange = scroll.MinimalChange;
    }

    private static void TimeoutProperty_OnChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
        if (d is not DynamicScrollViewer scroll)
            return;

        scroll._timeout = scroll.Timeout;
    }
}
