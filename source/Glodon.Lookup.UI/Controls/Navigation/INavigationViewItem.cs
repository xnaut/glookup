﻿ 
// Based on Windows UI Library
// Copyright(c) Microsoft Corporation.All rights reserved.

// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Collections;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;

namespace Wpf.Ui.Controls.Navigation;


public interface INavigationViewItem
{
    
    string Id { get; }

    
    object Content { get; set; }

    
    object? Icon { get; set; }

    
    IList MenuItems { get; set; }

    
    object? MenuItemsSource { get; set; }

    
    bool IsActive { get; }

    
    bool IsExpanded { get; internal set; }

    
    string TargetPageTag { get; set; }

    
    Type? TargetPageType { get; set; }

    
    ControlTemplate? Template { get; set; }

    
    INavigationViewItem? NavigationViewItemParent { get; internal set; }

    
    [Category("Behavior")]
    event RoutedEventHandler Click;

    internal bool IsMenuElement { get; set; }

    
    void Activate();

    
    void Deactivate();
}

