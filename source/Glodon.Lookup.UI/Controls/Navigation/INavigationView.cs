﻿ 
// Based on Windows UI Library
// Copyright(c) Microsoft Corporation.All rights reserved.

// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Collections;
using System.Windows;
using System.Windows.Controls;
using Wpf.Ui.Animations;
using Wpf.Ui.Common;
using Wpf.Ui.Contracts;
using Wpf.Ui.Controls.AutoSuggestBoxControl;
using Wpf.Ui.Controls.TitleBarControl;

namespace Wpf.Ui.Controls.Navigation;


public interface INavigationView
{
    
    object? Header { get; set; }

    
    Visibility HeaderVisibility { get; set; }

    
    bool AlwaysShowHeader { get; set; }

    
    IList MenuItems { get; set; }

    
    object? MenuItemsSource { get; set; }

    
    IList FooterMenuItems { get; set; }

    
    object? FooterMenuItemsSource { get; set; }

    
    INavigationViewItem? SelectedItem { get; }

    
    object? ContentOverlay { get; set; }

    
    bool IsBackEnabled { get; }

    
    NavigationViewBackButtonVisible IsBackButtonVisible { get; set; }

    
    bool IsPaneToggleVisible { get; set; }

    
    bool IsPaneOpen { get; set; }

    
    bool IsPaneVisible { get; set; }

    
    double OpenPaneLength { get; set; }

    
    double CompactPaneLength { get; set; }

    
    object? PaneHeader { get; set; }

    
    string? PaneTitle { get; set; }

    
    object? PaneFooter { get; set; }

    
    NavigationViewPaneDisplayMode PaneDisplayMode { get; set; }

    
    AutoSuggestBox? AutoSuggestBox { get; set; }

    
    TitleBar? TitleBar { get; set; }

    
    ControlTemplate? ItemTemplate { get; set; }

    
    int TransitionDuration { get; set; }

    
    TransitionType TransitionType { get; set; }

    
    Thickness FrameMargin { get; set; }

    
    event TypedEventHandler<NavigationView, RoutedEventArgs> PaneOpened;

    
    event TypedEventHandler<NavigationView, RoutedEventArgs> PaneClosed;

    
    event TypedEventHandler<NavigationView, RoutedEventArgs> SelectionChanged;

    
    event TypedEventHandler<NavigationView, RoutedEventArgs> ItemInvoked;

    
    event TypedEventHandler<NavigationView, RoutedEventArgs> BackRequested;

    
    event TypedEventHandler<NavigationView, NavigatingCancelEventArgs> Navigating;

    
    bool CanGoBack { get; }

    
    bool Navigate(Type pageType, object? dataContext = null);

    
    bool Navigate(string pageIdOrTargetTag, object? dataContext = null);

    
    /// <param name="pageType"></param>
    /// <param name="dataContext"></param>
    /// <returns></returns>
    bool NavigateWithHierarchy(Type pageType, object? dataContext = null);

    
    bool ReplaceContent(Type pageTypeToEmbed);

    
    bool ReplaceContent(UIElement pageInstanceToEmbed, object? dataContext = null);

    
    /// <returns><see langword="true"/> if successfully navigated forward, otherwise <see langword="false"/>.</returns>
    bool GoForward();

    
    /// <returns><see langword="true"/> if successfully navigated backward, otherwise <see langword="false"/>.</returns>
    bool GoBack();

    
    void ClearJournal();

    
    void SetPageService(IPageService pageService);

    
    void SetServiceProvider(IServiceProvider serviceProvider);
}
