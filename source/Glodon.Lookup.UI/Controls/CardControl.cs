﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.ComponentModel;
using System.Windows;
using System.Windows.Media;

namespace Wpf.Ui.Controls;


public class CardControl : System.Windows.Controls.Primitives.ButtonBase, IIconControl
{
    
    public static readonly DependencyProperty HeaderProperty = DependencyProperty.Register(nameof(Header),
        typeof(object), typeof(CardControl), new PropertyMetadata(null));

    
    public static readonly DependencyProperty IconProperty = DependencyProperty.Register(nameof(Icon),
        typeof(Common.SymbolRegular), typeof(CardControl),
        new PropertyMetadata(Common.SymbolRegular.Empty));

    
    public static readonly DependencyProperty IconFilledProperty = DependencyProperty.Register(nameof(IconFilled),
        typeof(bool), typeof(CardControl), new PropertyMetadata(false));

    
    public static readonly DependencyProperty IconForegroundProperty = DependencyProperty.Register(nameof(IconForeground),
        typeof(Brush), typeof(CardControl), new FrameworkPropertyMetadata(SystemColors.ControlTextBrush,
            FrameworkPropertyMetadataOptions.Inherits));

    // Using a DependencyProperty as the backing store for CardControlIconSize.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty CardControlIconSizeProperty =
        DependencyProperty.Register("CardControlIconSize", typeof(double), typeof(CardControl), new PropertyMetadata(20.0));


    
    [Bindable(true)]
    public object Header
    {
        get => GetValue(HeaderProperty);
        set => SetValue(HeaderProperty, value);
    }

    [Bindable(true), Category("Appearance")]
    public Common.SymbolRegular Icon
    {
        get => (Common.SymbolRegular)GetValue(IconProperty);
        set => SetValue(IconProperty, value);
    }

    [Bindable(true), Category("Appearance")]
    public bool IconFilled
    {
        get => (bool)GetValue(IconFilledProperty);
        set => SetValue(IconFilledProperty, value);
    }

    
    [Bindable(true), Category("Appearance")]
    public Brush IconForeground
    {
        get => (Brush)GetValue(IconForegroundProperty);
        set => SetValue(IconForegroundProperty, value);
    }


    [Bindable(true), Category("Appearance")]
    public double CardControlIconSize
    {
        get { return (double)GetValue(CardControlIconSizeProperty); }
        set { SetValue(CardControlIconSizeProperty, value); }
    }

}
