﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.ComponentModel;
using System.Drawing;
using System.Windows;
using Wpf.Ui.Common;
using Wpf.Ui.Controls.Window;
using Wpf.Ui.Interop;

namespace Wpf.Ui.Controls.MessageBoxControl;


[ToolboxItem(true)]
[ToolboxBitmap(typeof(MessageBox), "MessageBox.bmp")]
public class MessageBox : System.Windows.Window
{
    #region Static properties

    
    public static readonly DependencyProperty ShowTitleProperty = DependencyProperty.Register(nameof(ShowTitle),
        typeof(bool), typeof(MessageBox), new PropertyMetadata(true));

    
    public static readonly DependencyProperty PrimaryButtonTextProperty = DependencyProperty.Register(
        nameof(PrimaryButtonText),
        typeof(string), typeof(MessageBox), new PropertyMetadata(string.Empty));

    
    public static readonly DependencyProperty SecondaryButtonTextProperty = DependencyProperty.Register(
        nameof(SecondaryButtonText),
        typeof(string), typeof(MessageBox), new PropertyMetadata(string.Empty));

    
    public static readonly DependencyProperty CloseButtonTextProperty = DependencyProperty.Register(
        nameof(CloseButtonText),
        typeof(string), typeof(MessageBox), new PropertyMetadata("Close"));

    
    public static readonly DependencyProperty PrimaryButtonIconProperty =
        DependencyProperty.Register(nameof(PrimaryButtonIcon),
            typeof(SymbolRegular), typeof(MessageBox), new PropertyMetadata(SymbolRegular.Empty));

    
    public static readonly DependencyProperty SecondaryButtonIconProperty =
        DependencyProperty.Register(nameof(SecondaryButtonIcon),
            typeof(SymbolRegular), typeof(MessageBox), new PropertyMetadata(SymbolRegular.Empty));

    
    public static readonly DependencyProperty CloseButtonIconProperty =
        DependencyProperty.Register(nameof(CloseButtonIcon),
            typeof(SymbolRegular), typeof(MessageBox), new PropertyMetadata(SymbolRegular.Empty));

    
    public static readonly DependencyProperty PrimaryButtonAppearanceProperty = DependencyProperty.Register(
        nameof(PrimaryButtonAppearance),
        typeof(ControlAppearance), typeof(MessageBox),
        new PropertyMetadata(ControlAppearance.Primary));

    
    public static readonly DependencyProperty SecondaryButtonAppearanceProperty = DependencyProperty.Register(
        nameof(SecondaryButtonAppearance),
        typeof(ControlAppearance), typeof(MessageBox),
        new PropertyMetadata(ControlAppearance.Secondary));

    
    public static readonly DependencyProperty CloseButtonAppearanceProperty = DependencyProperty.Register(
        nameof(CloseButtonAppearance),
        typeof(ControlAppearance), typeof(MessageBox),
        new PropertyMetadata(ControlAppearance.Secondary));

    
    public static readonly DependencyProperty IsPrimaryButtonEnabledProperty = DependencyProperty.Register(
        nameof(IsPrimaryButtonEnabled),
        typeof(bool), typeof(MessageBox),
        new PropertyMetadata(true));

    
    public static readonly DependencyProperty IsSecondaryButtonEnabledProperty = DependencyProperty.Register(
        nameof(IsSecondaryButtonEnabled),
        typeof(bool), typeof(MessageBox),
        new PropertyMetadata(true));

    
    public static readonly DependencyProperty TemplateButtonCommandProperty =
        DependencyProperty.Register(nameof(TemplateButtonCommand),
            typeof(IRelayCommand), typeof(MessageBox), new PropertyMetadata(null));

    #endregion

    #region Properties

    
    public bool ShowTitle
    {
        get => (bool)GetValue(ShowTitleProperty);
        set => SetValue(ShowTitleProperty, value);
    }

    
    public string PrimaryButtonText
    {
        get => (string)GetValue(PrimaryButtonTextProperty);
        set => SetValue(PrimaryButtonTextProperty, value);
    }

    
    public string SecondaryButtonText
    {
        get => (string)GetValue(SecondaryButtonTextProperty);
        set => SetValue(SecondaryButtonTextProperty, value);
    }

    
    public string CloseButtonText
    {
        get => (string)GetValue(CloseButtonTextProperty);
        set => SetValue(CloseButtonTextProperty, value);
    }

    
    public SymbolRegular PrimaryButtonIcon
    {
        get => (SymbolRegular)GetValue(PrimaryButtonIconProperty);
        set => SetValue(PrimaryButtonIconProperty, value);
    }

    
    public SymbolRegular SecondaryButtonIcon
    {
        get => (SymbolRegular)GetValue(SecondaryButtonIconProperty);
        set => SetValue(SecondaryButtonIconProperty, value);
    }

    
    public SymbolRegular CloseButtonIcon
    {
        get => (SymbolRegular)GetValue(CloseButtonIconProperty);
        set => SetValue(CloseButtonIconProperty, value);
    }

    
    public ControlAppearance PrimaryButtonAppearance
    {
        get => (ControlAppearance)GetValue(PrimaryButtonAppearanceProperty);
        set => SetValue(PrimaryButtonAppearanceProperty, value);
    }

    
    public ControlAppearance SecondaryButtonAppearance
    {
        get => (ControlAppearance)GetValue(SecondaryButtonAppearanceProperty);
        set => SetValue(SecondaryButtonAppearanceProperty, value);
    }

    
    public ControlAppearance CloseButtonAppearance
    {
        get => (ControlAppearance)GetValue(CloseButtonAppearanceProperty);
        set => SetValue(CloseButtonAppearanceProperty, value);
    }

    
    public bool IsSecondaryButtonEnabled
    {
        get => (bool)GetValue(IsSecondaryButtonEnabledProperty);
        set => SetValue(IsSecondaryButtonEnabledProperty, value);
    }

    
    public bool IsPrimaryButtonEnabled
    {
        get => (bool)GetValue(IsPrimaryButtonEnabledProperty);
        set => SetValue(IsPrimaryButtonEnabledProperty, value);
    }

    
    public IRelayCommand TemplateButtonCommand => (IRelayCommand)GetValue(TemplateButtonCommandProperty);

    #endregion

    public MessageBox()
    {
        Topmost = true;
        SetValue(TemplateButtonCommandProperty, new RelayCommand<MessageBoxButton>(OnTemplateButtonClick));
    }

    protected TaskCompletionSource<MessageBoxResult>? Tcs;

    [Obsolete($"Use {nameof(ShowDialogAsync)} instead")]
    public new void Show()
    {
        throw new InvalidOperationException($"Use {nameof(ShowDialogAsync)} instead");
    }

    [Obsolete($"Use {nameof(ShowDialogAsync)} instead")]
    public new bool? ShowDialog()
    {
        RemoveTitleBarAndApplyMica();
        return base.ShowDialog();
    }

    
    /// <returns><see cref="MessageBoxResult"/></returns>
    /// <exception cref="TaskCanceledException"></exception>
    public async Task<MessageBoxResult> ShowDialogAsync(CancellationToken cancellationToken = default)
    {
        Tcs = new TaskCompletionSource<MessageBoxResult>();
        var tokenRegistration = cancellationToken.Register(o => Tcs.TrySetCanceled((CancellationToken)o!), cancellationToken);

        try
        {
#pragma warning disable CS0618
            ShowDialog();
#pragma warning restore CS0618

            return await Tcs!.Task;
        }
        finally
        {
#if NET6_0_OR_GREATER
            await tokenRegistration.DisposeAsync();
#else
            tokenRegistration.Dispose();
#endif
        }
    }

    protected override void OnInitialized(EventArgs e)
    {
        base.OnInitialized(e);

        PreviewMouseDoubleClick += static (_, args) => args.Handled = true;

        Loaded += static (sender, _) =>
        {
            var self = (MessageBox)sender;

            if (self.VisualChildrenCount <= 0 || self.GetVisualChild(0) is not UIElement content)
                return;

            self.ResizeToContentSize(content);
            self.CenterWindowOnScreen();
        };
    }

    protected override void OnClosing(CancelEventArgs e)
    {
        base.OnClosing(e);

        if (e.Cancel)
            return;

        Tcs?.TrySetResult(MessageBoxResult.None);
    }

    
    /// <param name="content"></param>
    protected virtual void ResizeToContentSize(UIElement content)
    {
        //left and right margin
        const double margin = 12.0 * 2;

        Width = content.DesiredSize.Width + margin;
        Height = content.DesiredSize.Height;

        while (true)
        {
            if (Width <= MaxWidth && Height <= MaxHeight)
                break;

            if (Width > MaxWidth)
            {
                Width = MaxWidth;
                content.UpdateLayout();

                Height = content.DesiredSize.Height;
            }

            if (Height > MaxHeight)
            {
                Height = MaxHeight;
                content.UpdateLayout();

                Width = content.DesiredSize.Width;
            }
        }
    }

    protected virtual void CenterWindowOnScreen()
    {
        //TODO MessageBox should be displayed on the window on which the application

        double screenWidth = SystemParameters.PrimaryScreenWidth;
        double screenHeight = SystemParameters.PrimaryScreenHeight;

        Left = (screenWidth / 2) - (Width / 2);
        Top = (screenHeight / 2) - (Height / 2);
    }

    
    /// <param name="button"></param>
    /// <returns>
    /// 
    /// </returns>
    protected virtual bool OnButtonClick(MessageBoxButton button) { return true; }

    private void RemoveTitleBarAndApplyMica()
    {
        UnsafeNativeMethods.RemoveWindowTitlebarContents(this);
        WindowBackdrop.ApplyBackdrop(this, WindowBackdropType.Mica);
    }

    private void OnTemplateButtonClick(MessageBoxButton button)
    {
        if (!OnButtonClick(button))
            return;

        MessageBoxResult result = button switch
        {
            MessageBoxButton.Primary => MessageBoxResult.Primary,
            MessageBoxButton.Secondary => MessageBoxResult.Secondary,
            _ => MessageBoxResult.None
        };

        Tcs?.TrySetResult(result);
        Close();
    }
}
