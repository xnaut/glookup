﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski, Ch0pstix and WPF UI Contributors.
// All Rights Reserved.

using System.Windows;
using System.Windows.Interop;
using Wpf.Ui.Controls.Window;

namespace Wpf.Ui.Appearance;

// https://github.com/lepoco/wpfui/issues/55


public sealed class Watcher
{
    
    public WindowBackdropType BackgroundEffect { get; set; } = WindowBackdropType.None;

    
    public bool UpdateAccents { get; set; } = false;
    public bool ForceBackground { get; set; } = false;

    /// <param name="window">The window that will be updated by <see cref="Watcher"/>.</param>
    /// <param name="backgroundEffect">Background effect to be applied when changing the theme.</param>
    /// <param name="updateAccents">If <see langword="true"/>, the accents will be updated when the change is detected.</param>
    /// <param name="forceBackground">If <see langword="true"/>, bypasses the app's theme compatibility check and tries to force the change of a background effect.</param>
    public static void Watch(Window window, WindowBackdropType backgroundEffect = WindowBackdropType.Mica,
        bool updateAccents = true, bool forceBackground = false)
    {
        if (window == null)
            return;

        if (window.IsLoaded)
        {
            // Get the handle from the window
            IntPtr hwnd =
                (hwnd = new WindowInteropHelper(window).Handle) == IntPtr.Zero
                    ? throw new InvalidOperationException("Could not get window handle.")
                    : hwnd;

            // Initialize a new instance with the window handle
            var watcher = new Watcher(hwnd, backgroundEffect, updateAccents, forceBackground);

            // Updates themes on initialization if the current system theme is different from the app's.
            var currentSystemTheme = SystemTheme.GetTheme();
            watcher.UpdateThemes(currentSystemTheme);

            return;
        }

        window.Loaded += (sender, args) =>
        {
            // Get the handle from the window
            IntPtr hwnd =
                (hwnd = new WindowInteropHelper(window).Handle) == IntPtr.Zero
                    ? throw new InvalidOperationException("Could not get window handle.")
                    : hwnd;

            // Initialize a new instance with the window handle
            var watcher = new Watcher(hwnd, backgroundEffect, updateAccents, forceBackground);

            // Updates themes on initialization if the current system theme is different from the app's.
            var currentSystemTheme = SystemTheme.GetTheme();
            watcher.UpdateThemes(currentSystemTheme);
        };
    }

    
    /// <param name="hWnd">Window handle</param>
    /// <param name="backgroundEffect">Background effect to be applied when changing the theme.</param>
    /// <param name="updateAccents">If <see langword="true"/>, the accents will be updated when the change is detected.</param>
    /// <param name="forceBackground">If <see langword="true"/>, bypasses the app's theme compatibility check and tries to force the change of a background effect.</param>
    public Watcher(IntPtr hWnd, WindowBackdropType backgroundEffect, bool updateAccents, bool forceBackground)
    {
        var hWndSource = HwndSource.FromHwnd(hWnd);

        BackgroundEffect = backgroundEffect;
        ForceBackground = forceBackground;
        UpdateAccents = updateAccents;

        hWndSource?.AddHook(WndProc);
    }

    
    private IntPtr WndProc(IntPtr hWnd, int msg, IntPtr wParam, IntPtr lParam, ref bool handled)
    {
        if (msg != (int)Interop.User32.WM.WININICHANGE)
            return IntPtr.Zero;

        var currentSystemTheme = SystemTheme.GetTheme();
        UpdateThemes(currentSystemTheme);

        return IntPtr.Zero;
    }

    private void UpdateThemes(SystemThemeType systemTheme)
    {
        AppearanceData.SystemTheme = systemTheme;

        var themeToSet = ThemeType.Light;

        if (systemTheme is SystemThemeType.Dark or SystemThemeType.CapturedMotion or SystemThemeType.Glow)
            themeToSet = ThemeType.Dark;

        Theme.Apply(Application.MainWindow, themeToSet, BackgroundEffect, UpdateAccents, ForceBackground);

#if DEBUG
        System.Diagnostics.Debug.WriteLine($"INFO | {typeof(Watcher)} changed the app theme.", "Wpf.Ui.Watcher");
        System.Diagnostics.Debug.WriteLine($"INFO | Current accent: {Accent.SystemAccent}", "Wpf.Ui.Watcher");
        System.Diagnostics.Debug.WriteLine($"INFO | Current app theme: {AppearanceData.ApplicationTheme}",
            "Wpf.Ui.Watcher");
        System.Diagnostics.Debug.WriteLine($"INFO | Current system theme: {AppearanceData.SystemTheme}",
            "Wpf.Ui.Watcher");
#endif
    }
}
