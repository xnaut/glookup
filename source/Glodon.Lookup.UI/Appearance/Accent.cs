﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Windows;
using System.Windows.Media;
using Wpf.Ui.Extensions;
using Wpf.Ui.Interop;

namespace Wpf.Ui.Appearance;

public static class Accent
{
    private const double BackgroundBrightnessThresholdValue = 80d;
    
    public static Color SystemAccent
    {
        get
        {
            var resource = Application.MainWindow.Resources["SystemAccentColor"];

            if (resource is Color color)
                return color;

            return Colors.Transparent;
        }
    }

    
    public static Brush SystemAccentBrush => new SolidColorBrush(SystemAccent);

    
    public static Color PrimaryAccent
    {
        get
        {
            var resource = Application.MainWindow.Resources["SystemAccentColorPrimary"];

            if (resource is Color color)
                return color;

            return Colors.Transparent;
        }
    }

    
    public static Brush PrimaryAccentBrush => new SolidColorBrush(PrimaryAccent);

    
    public static Color SecondaryAccent
    {
        get
        {
            var resource = Application.MainWindow.Resources["SystemAccentColorSecondary"];

            if (resource is Color color)
                return color;

            return Colors.Transparent;
        }
    }

    
    public static Brush SecondaryAccentBrush => new SolidColorBrush(SecondaryAccent);

    
    public static Color TertiaryAccent
    {
        get
        {
            var resource = Application.MainWindow.Resources["SystemAccentColorTertiary"];

            if (resource is Color color)
                return color;

            return Colors.Transparent;
        }
    }

    
    public static Brush TertiaryAccentBrush => new SolidColorBrush(TertiaryAccent);

    
    /// <param name="element">Framework element</param>
    /// <param name="systemAccent">Primary accent color.</param>
    /// <param name="themeType">If <see cref="ThemeType.Dark"/>, the colors will be different.</param>
    /// <param name="systemGlassColor">If the color is taken from the Glass Color System, its brightness will be increased with the help of the operations on HSV space.</param>
    public static void Apply(FrameworkElement element, Color systemAccent, ThemeType themeType = ThemeType.Light, bool systemGlassColor = false)
    {
        if (systemGlassColor)
        {
            // WindowGlassColor is little darker than accent color
            systemAccent = systemAccent.UpdateBrightness(6f);
        }

        Color primaryAccent, secondaryAccent, tertiaryAccent;

        if (themeType == ThemeType.Dark)
        {
            primaryAccent = systemAccent.Update(15f, -12f);
            secondaryAccent = systemAccent.Update(30f, -24f);
            tertiaryAccent = systemAccent.Update(45f, -36f);
        }
        else
        {
            primaryAccent = systemAccent.UpdateBrightness(-5f);
            secondaryAccent = systemAccent.UpdateBrightness(-10f);
            tertiaryAccent = systemAccent.UpdateBrightness(-15f);
        }

        UpdateColorResources(
            element,
            systemAccent,
            primaryAccent,
            secondaryAccent,
            tertiaryAccent
        );
    }

    
    /// <param name="element">Framework element</param>
    /// <param name="systemAccent">Primary color.</param>
    /// <param name="primaryAccent">Alternative light or dark color.</param>
    /// <param name="secondaryAccent">Second alternative light or dark color (most used).</param>
    /// <param name="tertiaryAccent">Third alternative light or dark color.</param>
    public static void Apply(FrameworkElement element, Color systemAccent, Color primaryAccent, Color secondaryAccent, Color tertiaryAccent)
    {
        UpdateColorResources(element, systemAccent, primaryAccent, secondaryAccent, tertiaryAccent);
    }

    
    public static void ApplySystemAccent(FrameworkElement element)
    {
        Apply(element, GetColorizationColor(), Theme.GetAppTheme());
    }

    
    public static Color GetColorizationColor()
    {
        return UnsafeNativeMethods.GetDwmColor();
    }

    
    private static void UpdateColorResources(FrameworkElement element, Color systemAccent, Color primaryAccent, Color secondaryAccent, Color tertiaryAccent)
    {
#if DEBUG
        System.Diagnostics.Debug.WriteLine("INFO | SystemAccentColor: " + systemAccent, "Wpf.Ui.Accent");
        System.Diagnostics.Debug.WriteLine("INFO | SystemAccentColorPrimary: " + primaryAccent, "Wpf.Ui.Accent");
        System.Diagnostics.Debug.WriteLine("INFO | SystemAccentColorSecondary: " + secondaryAccent, "Wpf.Ui.Accent");
        System.Diagnostics.Debug.WriteLine("INFO | SystemAccentColorTertiary: " + tertiaryAccent, "Wpf.Ui.Accent");
#endif

        if (secondaryAccent.GetBrightness() > BackgroundBrightnessThresholdValue)
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("INFO | Text on accent is DARK", "Wpf.Ui.Accent");
#endif
            element.Resources["TextOnAccentFillColorPrimary"] = Color.FromArgb(0xFF, 0x00, 0x00, 0x00);
            element.Resources["TextOnAccentFillColorSecondary"] = Color.FromArgb(0x80, 0x00, 0x00, 0x00);
            element.Resources["TextOnAccentFillColorDisabled"] = Color.FromArgb(0x77, 0x00, 0x00, 0x00);
            element.Resources["TextOnAccentFillColorSelectedText"] = Color.FromArgb(0x00, 0x00, 0x00, 0x00);
            element.Resources["AccentTextFillColorDisabled"] = Color.FromArgb(0x5D, 0x00, 0x00, 0x00);
        }
        else
        {
#if DEBUG
            System.Diagnostics.Debug.WriteLine("INFO | Text on accent is LIGHT", "Wpf.Ui.Accent");
#endif
            element.Resources["TextOnAccentFillColorPrimary"] = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            element.Resources["TextOnAccentFillColorSecondary"] = Color.FromArgb(0x80, 0xFF, 0xFF, 0xFF);
            element.Resources["TextOnAccentFillColorDisabled"] = Color.FromArgb(0x87, 0xFF, 0xFF, 0xFF);
            element.Resources["TextOnAccentFillColorSelectedText"] = Color.FromArgb(0xFF, 0xFF, 0xFF, 0xFF);
            element.Resources["AccentTextFillColorDisabled"] = Color.FromArgb(0x5D, 0xFF, 0xFF, 0xFF);
        }

        element.Resources["SystemAccentColor"] = systemAccent;
        element.Resources["SystemAccentColorPrimary"] = primaryAccent;
        element.Resources["SystemAccentColorSecondary"] = secondaryAccent;
        element.Resources["SystemAccentColorTertiary"] = tertiaryAccent;

        element.Resources["SystemAccentBrush"] = secondaryAccent.ToBrush();
        element.Resources["SystemFillColorAttentionBrush"] = secondaryAccent.ToBrush();
        element.Resources["AccentTextFillColorPrimaryBrush"] = tertiaryAccent.ToBrush();
        element.Resources["AccentTextFillColorSecondaryBrush"] = tertiaryAccent.ToBrush();
        element.Resources["AccentTextFillColorTertiaryBrush"] = secondaryAccent.ToBrush();
        element.Resources["AccentFillColorSelectedTextBackgroundBrush"] = systemAccent.ToBrush();
        element.Resources["AccentFillColorDefaultBrush"] = secondaryAccent.ToBrush();

        element.Resources["AccentFillColorSecondaryBrush"] = secondaryAccent.ToBrush(0.9);
        element.Resources["AccentFillColorTertiaryBrush"] = secondaryAccent.ToBrush(0.8);
    }
}
