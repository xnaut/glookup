﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Wpf.Ui.Contracts;


public interface INotifyIconService
{
    
    public int Id { get; }

    
    public bool IsRegistered { get; }

    
    public string TooltipText { get; set; }

    
    ContextMenu ContextMenu { get; set; }

    
    public ImageSource Icon { get; set; }

    
    public bool Register();

    
    public bool Unregister();

    
    public void SetParentWindow(Window window);
}

