﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using Wpf.Ui.Controls.Navigation;

namespace Wpf.Ui.Contracts;


public interface INavigationWindow
{
    
    /// <returns>Instance of the <see cref="INavigation"/> control.</returns>
    INavigationView GetNavigation();

    
    /// <param name="pageType"><see langword="Type"/> of the page.</param>
    bool Navigate(Type pageType);

    
    /// <param name="serviceProvider">Instance of the <see cref="IServiceProvider"/>.</param>
    void SetServiceProvider(IServiceProvider serviceProvider);

    
    /// <param name="pageService">Instance of the <see cref="IPageService"/> with attached service provider.</param>
    void SetPageService(IPageService pageService);

    
    void ShowWindow();

    
    void CloseWindow();
}
