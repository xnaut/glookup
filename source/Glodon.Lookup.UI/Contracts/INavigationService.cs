﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using Wpf.Ui.Controls.Navigation;

namespace Wpf.Ui.Contracts;


public interface INavigationService
{
    
    /// <returns>Instance of the <see cref="INavigation"/> control.</returns>
    INavigationView GetNavigationControl();

    
    /// <param name="navigation">Instance of the <see cref="INavigation"/>.</param>
    void SetNavigationControl(INavigationView navigation);

    
    /// <param name="pageService">Instance of the <see cref="IPageService"/>.</param>
    void SetPageService(IPageService pageService);

    
    /// <param name="pageType"><see langword="Type"/> of the page.</param>
    bool Navigate(Type pageType);

    
    /// <param name="pageIdOrTargetTag">Id or tag of the page.</param>
    bool Navigate(string pageIdOrTargetTag);

    
    /// <returns></returns>
    bool GoBack();

    
    /// <param name="pageType"></param>
    /// <returns></returns>
    bool NavigateWithHierarchy(Type pageType);
}
