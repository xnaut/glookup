﻿ // This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Windows.Controls;
using Wpf.Ui.Controls.ContentDialogControl;

namespace Wpf.Ui.Contracts;


public interface IContentDialogService
{
    
    /// <param name="contentPresenter"></param>
    void SetContentPresenter(ContentPresenter contentPresenter);

    
    /// <returns></returns>
    ContentPresenter GetContentPresenter();

    
    /// <returns></returns>
    ContentDialog CreateDialog();
}
