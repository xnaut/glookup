﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Windows;
using Wpf.Ui.TaskBar;

namespace Wpf.Ui.Contracts;


public interface ITaskBarService
{
    
    /// <param name="hWnd">Window handle.</param>
    TaskBarProgressState GetState(IntPtr hWnd);

    
    /// <param name="window">Selected window.</param>
    TaskBarProgressState GetState(Window window);

    
    /// <param name="progressState">Progress sate to set.</param>
    //bool SetState(ProgressState progressState);

    
    /// <param name="current">Current value to display.</param>
    /// <param name="max">Maximum number for division.</param>
    //bool SetValue(int current, int max);

    
    /// <param name="hWnd">Window handle to modify.</param>
    /// <param name="taskBarProgressState">Progress sate to set.</param>
    bool SetState(IntPtr hWnd, TaskBarProgressState taskBarProgressState);

    
    /// <param name="hWnd">Window handle to modify.</param>
    /// <param name="taskBarProgressState">Progress sate to set.</param>
    /// <param name="current">Current value to display.</param>
    /// <param name="total">Maximum number for division.</param>
    bool SetValue(IntPtr hWnd, TaskBarProgressState taskBarProgressState, int current, int total);

    
    /// <param name="hWnd">Window handle to modify.</param>
    /// <param name="current">Current value to display.</param>
    /// <param name="max">Maximum number for division.</param>
    bool SetValue(IntPtr hWnd, int current, int max);

    
    /// <param name="window">Window to modify.</param>
    /// <param name="taskBarProgressState">Progress sate to set.</param>
    bool SetState(Window window, TaskBarProgressState taskBarProgressState);

    
    /// <param name="window">Window to modify.</param>
    /// <param name="taskBarProgressState">Progress sate to set.</param>
    /// <param name="current">Current value to display.</param>
    /// <param name="total">Maximum number for division.</param>
    bool SetValue(Window window, TaskBarProgressState taskBarProgressState, int current, int total);

    
    /// <param name="window">Window to modify.</param>
    /// <param name="current">Current value to display.</param>
    /// <param name="total">Maximum number for division.</param>
    bool SetValue(Window window, int current, int total);
}

