﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Windows.Media;
using Wpf.Ui.Appearance;

namespace Wpf.Ui.Contracts;


public interface IThemeService
{
    
    ThemeType GetTheme();

    
    ThemeType GetSystemTheme();

    
    SystemThemeType GetNativeSystemTheme();

    
    /// <param name="themeType">Theme type to set.</param>
    bool SetTheme(ThemeType themeType);

    
    bool SetSystemAccent();

    
    bool SetAccent(Color accentColor);

    
    bool SetAccent(SolidColorBrush accentSolidBrush);
}

