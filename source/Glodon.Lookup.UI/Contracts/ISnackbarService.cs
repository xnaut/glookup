﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using Wpf.Ui.Common;
using Wpf.Ui.Controls;

namespace Wpf.Ui.Contracts;


public interface ISnackbarService
{
    
    bool IsShown { get; }

    
    int Timeout { get; set; }

    
    void SetSnackbarControl(ISnackbarControl snackbar);

    
    ISnackbarControl GetSnackbarControl();

    
    /// <returns><see langword="true"/> if invocation of <see langword="async"/> method succeeded, Exception otherwise.</returns>
    bool Show();

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <returns><see langword="true"/> if invocation of <see langword="async"/> method succeeded, Exception otherwise.</returns>
    bool Show(string title);

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <param name="message"><see cref="ISnackbarControl.Message"/> in the content of the snackbar.</param>
    /// <returns><see langword="true"/> if invocation of <see langword="async"/> method succeeded, Exception otherwise.</returns>
    bool Show(string title, string message);

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <param name="message"><see cref="ISnackbarControl.Message"/> in the content of the snackbar.</param>
    /// <param name="icon">Icon on the left.</param>
    /// <returns><see langword="true"/> if invocation of <see langword="async"/> method succeeded, Exception otherwise.</returns>
    bool Show(string title, string message, SymbolRegular icon);

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <param name="message"><see cref="ISnackbarControl.Message"/> in the content of the snackbar.</param>
    /// <param name="icon"><see cref="IIconControl.Icon"/>Icon on the left.</param>
    /// <param name="appearance"><see cref="IAppearanceControl.Appearance"/> of the snackbar.</param>
    /// <returns><see langword="true"/> if invocation of <see langword="async"/> method succeeded, Exception otherwise.</returns>
    bool Show(string title, string message, SymbolRegular icon, ControlAppearance appearance);

    
    /// <returns><see langword="true"/> if the operation was successful.</returns>
    Task<bool> ShowAsync();

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <returns><see langword="true"/> if the operation was successful.</returns>
    Task<bool> ShowAsync(string title);

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <param name="message"><see cref="ISnackbarControl.Message"/> in the content of the snackbar.</param>
    /// <returns><see langword="true"/> if the operation was successful.</returns>
    Task<bool> ShowAsync(string title, string message);

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <param name="message"><see cref="ISnackbarControl.Message"/> in the content of the snackbar.</param>
    /// <param name="icon"><see cref="IIconControl.Icon"/> on the left.</param>
    /// <returns><see langword="true"/> if the operation was successful.</returns>
    Task<bool> ShowAsync(string title, string message, SymbolRegular icon);

    
    /// <param name="title"><see cref="ISnackbarControl.Title"/> at the top of the snackbar.</param>
    /// <param name="message"><see cref="ISnackbarControl.Message"/> in the content of the snackbar.</param>
    /// <param name="icon"><see cref="IIconControl.Icon"/> on the left.</param>
    /// <param name="appearance"><see cref="IAppearanceControl.Appearance"/> of the snackbar.</param>
    /// <returns><see langword="true"/> if the operation was successful.</returns>
    Task<bool> ShowAsync(string title, string message, SymbolRegular icon, ControlAppearance appearance);

    
    /// <returns><see langword="true"/> if invocation of <see langword="async"/> method succeeded, Exception otherwise.</returns>
    bool Hide();

    
    /// <returns><see langword="true"/> if the operation was successful.</returns>
    Task<bool> HideAsync();
}
