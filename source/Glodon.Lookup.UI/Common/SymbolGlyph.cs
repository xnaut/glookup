﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

namespace Wpf.Ui.Common;


public static class SymbolGlyph
{
    
    public const SymbolRegular DefaultIcon = SymbolRegular.BorderNone24;

    
    public const SymbolFilled DefaultFilledIcon = SymbolFilled.BorderNone24;

    
    /// <param name="name">Name of the icon.</param>
    public static Common.SymbolRegular Parse(string name)
    {
        if (String.IsNullOrEmpty(name))
            return DefaultIcon;

        try
        {
            return (SymbolRegular)Enum.Parse(typeof(SymbolRegular), name);
        }
        catch (Exception e)
        {
#if DEBUG
            throw;
#else
            return DefaultIcon;
#endif
        }
    }

    
    /// <param name="name">Name of the icon.</param>
    public static Common.SymbolFilled ParseFilled(string name)
    {
        if (String.IsNullOrEmpty(name))
            return DefaultFilledIcon;

        try
        {
            return (SymbolFilled)Enum.Parse(typeof(SymbolFilled), name);
        }
        catch (Exception e)
        {
#if DEBUG
            throw;
#else
            return DefaultFilledIcon;
#endif
        }
    }
}
