﻿ 
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using Wpf.Ui.Extensions;

namespace Wpf.Ui.Common;


/// </summary>
internal class EventIdentifier
{
    
    public long Current { get; internal set; } = 0;

    
    public long GetNext()
    {
        UpdateIdentifier();

        return Current;
    }

    
    public bool IsEqual(long storedId) => Current == storedId;

    
    private void UpdateIdentifier() => Current = DateTime.Now.GetMicroTimestamp();
}
