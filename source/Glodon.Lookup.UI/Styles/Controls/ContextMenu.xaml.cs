﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using System.Reflection;
using System.Windows;
using System.Windows.Threading;

namespace Wpf.Ui.Styles.Controls
{
    
    // This Code is based on a StackOverflow-Answer: https://stackoverflow.com/a/56736232/9759874
    partial class ContextMenu : ResourceDictionary
    {
        
        public ContextMenu()
        {
            // Run OnResourceDictionaryLoaded asynchronously to ensure other ResourceDictionary are already loaded before adding new entries
            Dispatcher.CurrentDispatcher.BeginInvoke(DispatcherPriority.Normal, new Action(OnResourceDictionaryLoaded));
        }

        private void OnResourceDictionaryLoaded()
        {
            var currentAssembly = typeof(Application).Assembly;

            AddEditorContextMenuDefaultStyle(currentAssembly);
        }

        private void AddEditorContextMenuDefaultStyle(Assembly currentAssembly)
        {
            var contextMenuStyle = this["UiContextMenu"] as Style;
            var editorContextMenuType = Type.GetType("System.Windows.Documents.TextEditorContextMenu+EditorContextMenu, " + currentAssembly);

            if (editorContextMenuType == null || contextMenuStyle == null)
                return;

            var editorContextMenuStyle = new Style(editorContextMenuType, contextMenuStyle);
            Add(editorContextMenuType, editorContextMenuStyle);
        }

    }
}
