﻿// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski and WPF UI Contributors.
// All Rights Reserved.

using Wpf.Ui.Common;

namespace Wpf.Ui.Extensions;


public static class SymbolExtensions
{
    
    public static SymbolFilled Swap(this SymbolRegular icon)
    {
        // It is possible that the alternative icon does not exist
        return SymbolGlyph.ParseFilled(icon.ToString());
    }

    
    public static SymbolRegular Swap(this SymbolFilled icon)
    {
        // It is possible that the alternative icon does not exist
        return SymbolGlyph.Parse(icon.ToString());
    }

    
    public static char GetGlyph(this SymbolRegular icon)
    {
        return ToChar(icon);
    }

    
    public static char GetGlyph(this SymbolFilled icon)
    {
        return ToChar(icon);
    }

    
    public static string GetString(this SymbolRegular icon)
    {
        return icon.GetGlyph().ToString();
    }

    
    public static string GetString(this SymbolFilled icon)
    {
        return icon.GetGlyph().ToString();
    }

    
    private static char ToChar(SymbolRegular icon)
    {
        return Convert.ToChar(icon);
    }

    
    private static char ToChar(SymbolFilled icon)
    {
        return Convert.ToChar(icon);
    }
}
