﻿ 
// This Source Code is partially based on reverse engineering of the Windows Operating System,
// and is intended for use on Windows systems only.
// This Source Code is partially based on the source code provided by the .NET Foundation.
// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) Leszek Pomianowski.
// All Rights Reserved.

// NOTE
// I split unmanaged code stuff into the NativeMethods library.
// If you have suggestions for the code below, please submit your changes there.
// https://github.com/lepoco/nativemethods

using System.Runtime.InteropServices;
using System.Text;

namespace Wpf.Ui.Interop;

// ReSharper disable IdentifierTypo
// ReSharper disable InconsistentNaming
internal static class UxTheme
{
    
    public struct MARGINS
    {
        
        public int cxLeftWidth;

        ///<summary>
        /// Width of right border that retains its size.
        /// </summary>
        public int cxRightWidth;

        
        public int cyTopHeight;

        
        public int cyBottomHeight;
    }

    
    public enum WINDOWTHEMEATTRIBUTETYPE : uint
    {
        
        WTA_NONCLIENT = 1,
    }

    
    [Flags]
    public enum WTNCA : uint
    {
        
        NODRAWCAPTION = 0x00000001,

        
        NODRAWICON = 0x00000002,

        
        NOSYSMENU = 0x00000004,

        
        NOMIRRORHELP = 0x00000008,

        
        VALIDBITS = NODRAWCAPTION | NODRAWICON | NOMIRRORHELP | NOSYSMENU,
    }

    
    [StructLayout(LayoutKind.Explicit)]
    public struct WTA_OPTIONS
    {
        // public static readonly uint Size = (uint)Marshal.SizeOf(typeof(WTA_OPTIONS));
        public const uint Size = 8;

        
        [FieldOffset(0)]
        public WTNCA dwFlags;

        
        [FieldOffset(4)]
        public WTNCA dwMask;
    }

    
    /// <param name="hWnd">
    /// Handle to a window to apply changes to.
    /// </param>
    /// <param name="eAttribute">
    /// Value of type WINDOWTHEMEATTRIBUTETYPE that specifies the type of attribute to set.
    /// The value of this parameter determines the type of data that should be passed in the pvAttribute parameter.
    /// Can be the following value:
    /// <list>WTA_NONCLIENT (Specifies non-client related attributes).</list>
    /// pvAttribute must be a pointer of type WTA_OPTIONS.
    /// </param>
    /// <param name="pvAttribute">
    /// A pointer that specifies attributes to set. Type is determined by the value of the eAttribute value.
    /// </param>
    /// <param name="cbAttribute">
    /// Specifies the size, in bytes, of the data pointed to by pvAttribute.
    /// </param>
    [DllImport(Libraries.UxTheme, PreserveSig = false)]
    public static extern void SetWindowThemeAttribute([In] IntPtr hWnd, [In] WINDOWTHEMEATTRIBUTETYPE eAttribute, [In] ref WTA_OPTIONS pvAttribute, [In] uint cbAttribute);

    
    /// <returns><see langword="true"/> if a visual style is enabled, and windows with visual styles applied should call OpenThemeData to start using theme drawing services.</returns>
    [DllImport(Libraries.UxTheme)]
    [return: MarshalAs(UnmanagedType.Bool)]
    public static extern bool IsThemeActive();

    
    /// <param name="pszThemeFileName">Pointer to a string that receives the theme path and file name.</param>
    /// <param name="dwMaxNameChars">Value of type int that contains the maximum number of characters allowed in the theme file name.</param>
    /// <param name="pszColorBuff">Pointer to a string that receives the color scheme name. This parameter may be set to NULL.</param>
    /// <param name="cchMaxColorChars">Value of type int that contains the maximum number of characters allowed in the color scheme name.</param>
    /// <param name="pszSizeBuff">Pointer to a string that receives the size name. This parameter may be set to NULL.</param>
    /// <param name="cchMaxSizeChars">Value of type int that contains the maximum number of characters allowed in the size name.</param>
    /// <returns>HRESULT</returns>
    [DllImport(Libraries.UxTheme, CharSet = CharSet.Unicode)]
    public static extern int GetCurrentThemeName(
        [Out] StringBuilder pszThemeFileName,
        [In] int dwMaxNameChars,
        [Out] StringBuilder pszColorBuff,
        [In] int cchMaxColorChars,
        [Out] StringBuilder pszSizeBuff,
        [In] int cchMaxSizeChars);
}
